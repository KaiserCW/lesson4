<?php

/*================ calculateSum – will accepts variable number of parameters and sum them all
                 *  using PHP's built-in funcs implode() (to glue elements of array into string and separate them with '+'),
                 *  and array_sum() (to sum all elements of array) */
function calculateSum($arr) {
    echo implode(' + ', $arr) . ' = ' . array_sum($arr) . PHP_EOL;
}

//================ multiply function – will accept two parameters and multiply them
function multiply($a, $b) {
    echo $a . ' * ' . $b . ' = ' . $a * $b . PHP_EOL;
}


$arrayOfNumbs = [45, 68, 1, 5, 7];
$numb1 = mt_rand(0, 1000);
$numb2 = mt_rand(0, 1000);


calculateSum($arrayOfNumbs);
multiply($numb1, $numb2);