<?php

//============== create function getRandomNumber to generate random numbers
function getRandomNumber() {
    return mt_rand(0, 100);
}

function calculateSum() {
    return implode(' + ', func_get_args()) . ' = ' . array_sum(func_get_args()) . PHP_EOL;
}

function multiply($a, $b) {
    if (is_numeric($a) && is_numeric($b)) {
        return $a . ' * ' . $b . ' = ' . $a * $b . PHP_EOL;
    }

    echo "Error: Wrong arguments! Please, type only numbers!";
    return false;
}

$numb1 = getRandomNumber();
$numb2 = getRandomNumber();

echo calculateSum(getRandomNumber(), getRandomNumber(), getRandomNumber(), getRandomNumber(), getRandomNumber());
echo multiply($numb1, $numb2);