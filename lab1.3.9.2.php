<?php

//====================== function that prints numbers from $n to zero
function recursiveCounter1($n) {
    if(is_int($n)){
        if ($n > 0) {
            echo $n-- . ' ';
            recursiveCounter1($n);
        } elseif ($n < 0) {
            echo $n++ . ' ';
            recursiveCounter1($n);
        } elseif ($n == 0) {
            echo $n . PHP_EOL;
        }
    } else {
        echo "Error: Type number!";
        return false;
    }
}

//====================== function that prints numbers from lower to larger
function recursiveCounter2($n1, $n2) {
    if (is_numeric($n1) && is_numeric($n2)) {
        if ($n1 < $n2) {
            echo $n1++ . ' ';
            recursiveCounter2($n1, $n2);
        } elseif ($n2 < $n1) {
            echo $n2++ . ' ';
            recursiveCounter2($n1, $n2);
        } else {
            echo $n1 . PHP_EOL;
        }
    } else {
        echo "Error: Type numbers!";
        return false;
    }
}

$numb1 = mt_rand(-50, 50);
$numb2 = mt_rand(-50, 50);

echo 'Testing recursiveCounter1():' . PHP_EOL, 'Number 1 = ' . $numb1 . PHP_EOL;
recursiveCounter1($numb1);

echo 'Testing recursiveCounter2():' . PHP_EOL, 'Number 1 = ' . $numb1 . PHP_EOL, 'Number 2 = ' . $numb2 . PHP_EOL;
recursiveCounter2($numb1, $numb2);