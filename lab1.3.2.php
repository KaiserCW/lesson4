<?php

/*$setUtf8 = `chcp 65001`;
echo $setUtf8;*/

$dev = 'программист';

$quantityOfDevs = mt_rand(0, 300);


//$suffix = ($quantityOfDevs % 10 == 1 && $quantityOfDevs != 11) ? '' : (($quantityOfDevs > 1 && $quantityOfDevs % 10 < 5) ? 'а' : 'ов');

//===================== In this case ternary operator is not best solution, so i decided to use branching for better readability
if ($quantityOfDevs) {
    if (($quantityOfDevs > 20) && ($quantityOfDevs % 10 > 1 && $quantityOfDevs % 10 < 5)) {
        $suffix = 'a';
    } elseif ($quantityOfDevs % 10 == 1 && $quantityOfDevs != 11) {
        $suffix = '';
    } else {
        $suffix = 'ов';
    }
    echo 'Над проектом работает ' . $quantityOfDevs . ' ' . $dev . $suffix;
} else {
    echo 'За проект пока никто не взялся.';
}



