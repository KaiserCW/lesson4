<?php

//======================== * Student should create two functions
function printHelloMessage() {
    return "Hello! Welcome again!";
}

function printGoodBayMessage() {
    return "Buy! Have a nice day!";
}

//======================== * Student should create variable $isWelcome with boolean value, which generated randomly
$isWelcome = (bool)mt_rand(0, 1);
//$isWelcome = mt_rand(0, 1) == 1;

//var_dump($isWelcome);

//======================== * In case of variable’s value, student should call one of early created functions
if ($isWelcome) {
    echo printHelloMessage();
} else {
    echo printGoodBayMessage();
}