<?php

$borderLine = '===================================';
$someStr = 'Experience is the teacher of fools.';


function drawDelimiter() {
    global $borderLine;
    echo $borderLine . PHP_EOL;
}

$drawContentString = function() use ($someStr) {
    echo $someStr . PHP_EOL;
};


echo drawDelimiter(), $drawContentString(), drawDelimiter();