<?php

$dayOfWeek = date('w');

switch($dayOfWeek) {
    case 1:
        $today = 'Monday'; break;
    case 2:
        $today = 'Tuesday'; break;
    case 3:
        $today = 'Wednesday'; break;
    case 4:
        $today = 'Thursday'; break;
    case 5:
        $today = 'Friday'; break;
    case 6:
    case 7:
        $today = 'Weekend'; break;
    default:
        $today = 'wrong day of week!';
}

echo 'It is ' . $today . ' today.';