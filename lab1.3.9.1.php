<?php

function calculateFactorial($n) {
    if (is_int($n) && $n > 0) {
        return $n * calculateFactorial($n - 1);
    } elseif ($n == 0) {
        return 1;
    }

    echo 'Error: argument must be a natural number greater than 0!';
    return false;
}

$intArg = mt_rand(0, 15);

echo 'Factorial of number ' . $intArg . ' = ' . calculateFactorial($intArg) . PHP_EOL;